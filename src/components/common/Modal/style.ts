import styled from 'styled-components';

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  gap: 15px;
  margin-top: 10px;
  
  .okButtonWrapper {
    width: 25%;
    font-size: 14px;
    height: 40px;
  }
  
  .cancelButtonWrapper {
    width: 25%;
    background: unset;
    color: black;
    border: 1px solid black;
    font-weight: unset;
    font-size: 14px;
    height: 40px;
  }
`;
