import React, { ReactNode } from 'react';
import { Modal } from 'antd';
import CustomButton from '../Button';
import { ButtonWrapper } from './style'

interface IModal {
  isOpen: boolean;
  handleOkButton: () => void;
  okButtonProps?: object;
  handleCancelButton: () => void;
  title: string;
  children: ReactNode;
  OkButtonDisable?: boolean;
}


const CustomModal = ({
  isOpen,
  handleOkButton,
  okButtonProps,
  handleCancelButton,
  title,
  children,
  OkButtonDisable
 }: IModal) => {

  return (
    <Modal
      title={title}
      open={isOpen}
      onOk={handleOkButton}
      onCancel={handleCancelButton}
      okButtonProps={okButtonProps}
      width={650}
      footer={null}
    >
      {children}
      <ButtonWrapper>
        <CustomButton label="Cancel" onClick={handleOkButton} customClass="cancelButtonWrapper" />
        <CustomButton label="Ok" onClick={handleOkButton} customClass="okButtonWrapper" disabled={OkButtonDisable} />
      </ButtonWrapper>
    </Modal>
  )

};

export default CustomModal;
