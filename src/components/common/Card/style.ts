import styled from 'styled-components';

export const CardWrapper = styled.div`
  border-radius: 2px;
  padding: 12px 10px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-content: center;
  align-items: center;
  max-height: 350px;
  box-shadow: 1px 1px 7px 0px gray;

  .title {
     font-size: 20px;
     font-weight: 600;
     color: black;
  }
  
  .price {
     font-size: 19px;
     font-weight: 700;
  }
`;

export const ImageWrapper = styled.img`
 height: 200px;
 width: 200px;
`;
