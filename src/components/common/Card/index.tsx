import React from 'react';
import { CardWrapper, ImageWrapper } from './style';
import CustomButton from '../Button';

interface ICard {
  title: string;
  price: number;
  onClick: () => void;
  image: string;
}

const CustomCard = ({
                      title,
                      price,
                      onClick,
                      image,
                    }: ICard) => {
  return (
    <CardWrapper>
      <ImageWrapper src={image} alt="alt" className="imageWrapper"/>
      <span className="title">{title}</span>
      <span className="price">{price}Rs.</span>
      <CustomButton label="Add To Cart" onClick={onClick} />
    </CardWrapper>
  )

};

export default CustomCard
