import styled from 'styled-components';

export const ButtonWrapper = styled.button`
  border: unset;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 12px 10px;
  height: 45px;
  font-size: 20px;
  width: 100%;
  cursor: pointer;
  background:  #e3aa4b;
  font-weight: 700;
  color: white;
`;
