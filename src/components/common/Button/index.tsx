import React from 'react';
import { ButtonWrapper } from './style'

interface IButton {
  label: string;
  onClick: () => void;
  customClass?: string;
  disabled?: boolean;
}

const CustomButton = ({label, onClick, customClass, disabled} : IButton) => {
  return  (
    <>
      <ButtonWrapper
        onClick={onClick}
        className={customClass}
        disabled={disabled}
      >
        {label}
      </ButtonWrapper>
    </>
  )
};

export default CustomButton
