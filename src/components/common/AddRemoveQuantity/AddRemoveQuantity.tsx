import React from 'react';
import CustomButton from '../Button';
import { AddRemoveQuantityWrapper } from './style';

interface IAddRemoveQuantity {
  removeOnClick: () => void;
  count: number;
  addOnClick: () => void;
}

const AddRemoveQuantity = ({removeOnClick, count, addOnClick}: IAddRemoveQuantity) =>{
  return(
    <AddRemoveQuantityWrapper>
      <CustomButton
        label="-"
        onClick={removeOnClick}
        customClass="buttonWrapper"
        disabled={count < 2}
      />
      {count}
      <CustomButton
        label="+"
        onClick={addOnClick}
        customClass="buttonWrapper"
      />
    </AddRemoveQuantityWrapper>
    )
};

export default AddRemoveQuantity;
