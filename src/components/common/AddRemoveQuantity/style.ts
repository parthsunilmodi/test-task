import styled from 'styled-components';

export const AddRemoveQuantityWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 10px;
  
  .buttonWrapper {
    padding: 5px;
    font-size: 14px;
    height: 20px;
    width: 20px;
    display: flex;
    align-items: center;
    justify-content: center;
    border: unset;
  }
`;
