import styled from 'styled-components';

export const ModalWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  border: 1px solid gainsboro;
  border-radius: 10px;
  align-items: center;
  gap: 25px;
  background: white;
  padding: 5px 20px
`;

export const RemoveButton = styled.div`
  cursor: pointer;
  text-decoration: underline;
  font-size: 10px;
  font-weight: 600;
`;

export const CartImage = styled.img`
  height: 45px;
  width: 45px;
  border-radius: 50%;
`;

export const QuantityWrapper = styled.div`
  display: flex;
  align-items: center;
  gap: 10px;
  
`;

export const ContainerWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 20px;
`;
