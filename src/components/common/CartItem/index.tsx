import React from 'react';
import AddRemoveQuantity from '../AddRemoveQuantity/AddRemoveQuantity';
import {
  CartImage,
  ModalWrapper,
  RemoveButton,
  QuantityWrapper,
  ContainerWrapper,
} from './style';

interface ICheckoutItem {
  imgUrl: string;
  price: string;
  name: string;
  type: string;
  imported: string;
  tax?: number;
  count?: number;
}

interface ICartItem {
  removeOnClick: () => void;
  addOnClick: () => void;
  removeButtonClick: () => void;
  item: ICheckoutItem;
}

const CartItem = ({
  removeOnClick,
  addOnClick,
  removeButtonClick,
  item,
                  }: ICartItem) => {
  return (
    <ModalWrapper>
      <ContainerWrapper>
        <CartImage src={item.imgUrl} alt="img" />
        <span>Name : {item.name}</span>
        <span>Price : {item.price} X {item.count} = {(parseFloat(item.price) * (item.count || 0)).toFixed(2)}</span>
      </ContainerWrapper>
      <QuantityWrapper>
        <AddRemoveQuantity
          removeOnClick={removeOnClick}
          count={item.count || 0}
          addOnClick={addOnClick}
        />
        <RemoveButton onClick={removeButtonClick}>REMOVE</RemoveButton>
      </QuantityWrapper>
    </ModalWrapper>
  )
};

export default CartItem
