import React from 'react';
import Logo from '../../../assets/image/icon/logo.jpg'
import AddToCart from '../../../assets/image/svg/addToCart.svg'
import { NavBarWrapper, AddCartWrapper, CountWrapper } from './style'

interface INavBar {
  handleCart: () => void;
  count: number;
}

const NavBar = ({ handleCart, count }: INavBar) => {
  return (
    <NavBarWrapper>
      <img src={Logo} alt="img" className="imgWrapper" />
      <AddCartWrapper onClick={handleCart}>
        <img src={AddToCart} alt="addToCart"/>
        {!!count && <CountWrapper>{count}</CountWrapper>}
      </AddCartWrapper>
    </NavBarWrapper>
  )
};

export default NavBar
