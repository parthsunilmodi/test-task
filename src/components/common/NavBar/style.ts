import styled from 'styled-components';

export const NavBarWrapper = styled.div`
  width: 100%;
  background: #FFFFFF;
  padding: 0 20px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  position: sticky;
  top: 0;
  box-shadow: 1px 1px 10px #000000;
  
  .imgWrapper {
    margin-left: 10%;
    height: 85px;
    width: 250px;
  }
`;

export const AddCartWrapper = styled.div`
 display: flex;
 align-items: center;
 justify-content: center;
 
  img {
    height: 25px;
    width: 25px;
  }
`;

export const CountWrapper = styled.span`
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  background: #e3aa4b;
  color: white;
  height: 20px;
  width: 20px;
  border-radius: 50%;
  font-weight: 700;
  font-size: 12px;
  padding: 5px;
  bottom:10px;
  right: 3px;
`;
