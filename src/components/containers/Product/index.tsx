import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { message } from 'antd';
import CustomCard from '../../common/Card';
import Data from './config.json';
import { addProduct, removeProduct, removeProductFromCart } from '../../../redux/action/product';
import { ProductWrapper, CartImage, ModalWrapper, ModalMainWrapper } from './style';
import NavBar from "../../common/NavBar";
import CustomModal from "../../common/Modal";
import CartItem from "../../common/CartItem";

interface ICheckoutItem {
  imgUrl: string;
  price: string;
  name: string;
  type: string;
  imported: string;
  tax?: number;
  count?: number;
}

const Product = () => {
  const dispatch: any = useDispatch();
  const { cartItem } = useSelector((state:any) => state.product);

  const [totalText, setTotalText] = useState<number>(0);
  const [checkoutData, setCheckoutData] = useState<any>();
  const [totalAmount, setTotalAmount] = useState<number>(0);
  const [modal,setModal] = useState<any>({
    cart: false,
    checkout: false
  });

  useEffect(() => {
    if(cartItem?.length) {
      const Array = cartItem?.map((item: ICheckoutItem) => {
        if ((item.type === "medicine" || item.type === "food") && !item.imported) {
          return {
            ...item,
            price: parseFloat(item.price) * (item?.count || 0),
            tax: 0
          }
        } else if ((item.type === "medicine" || item.type === "food") && item.imported) {
          return {
            ...item,
            price:  (parseFloat(item.price) * (item?.count || 0) * 5 / 100 + parseFloat(item.price) * (item?.count || 0)).toFixed(2),
            tax: (parseFloat(item.price) * (item?.count || 0) * 5 / 100).toFixed(2)
          }
        } else if (item.type === "other" && item.imported) {
          return {
            ...item,
            price: (parseFloat(item.price) * (item?.count || 0) * 15 / 100 + parseFloat(item.price) * (item?.count || 0)).toFixed(2),
            tax: (parseFloat(item.price) * (item?.count || 0) * 15 / 100).toFixed(2)
          };
        } else {
          return {
            ...item,
            price: (parseFloat(item.price) * (item?.count || 0) * 10 / 100 + parseFloat(item.price) * (item?.count || 0)).toFixed(2),
            tax: (parseFloat(item.price) * (item?.count || 0) * 10 / 100).toFixed(2)
          };
        }
      });
      setCheckoutData(Array);

      const initialValue = 0;
      const Tax = Array?.map((item: ICheckoutItem) => item.tax);
      const TotalTax = Tax?.reduce(
        (previousValue:string, currentValue: string) => (parseFloat(previousValue) + parseFloat(currentValue)).toFixed(2),
        initialValue
      );

      setTotalText(TotalTax);

      const Amount = Array?.map((item: ICheckoutItem) => item.price);
      const TotalAmount = Amount?.reduce(
        (previousValue:string, currentValue: string) => (parseFloat(previousValue) + parseFloat(currentValue)).toFixed(2),
        initialValue
      );
      setTotalAmount(TotalAmount);

    }
  }, [cartItem.length, cartItem]);

  const handleAddItem = (item : ICheckoutItem) => () => {
    message.success('Product Add SuccessFully', 1);
    dispatch(addProduct(item))
  };

  const handleRemoveItem = (item: ICheckoutItem) => () => {
    dispatch(removeProduct(item))
  };

  const handleRemoveItemFromCart = (item: ICheckoutItem) => () => {
    dispatch(removeProductFromCart(item))
  };

  const handleModal = (item: string) => () => {
    setModal({ ...modal, [item] : !modal[item] })
  };

  const handleCloseAllModal = () => {
    setModal({ cart: false, checkout: false })
  };

  const handleCartOkay = () => {
    handleModal('cart')();
    handleModal('checkout')();
  };

  return (
  <ProductWrapper>
    <NavBar count={cartItem.length || 0} handleCart={handleModal('cart')}  />
    <div className="cardWrapper">
      {
        Data?.data.map((item : any) =>
          <CustomCard
            title={item.name}
            price={item.price}
            onClick={handleAddItem(item)}
            image={item.imgUrl}
            {...item}
          />
        )
      }
    </div>
    <CustomModal
      isOpen={modal.cart}
      handleOkButton={handleCartOkay}
      handleCancelButton={handleModal('cart')}
      title="Add to Cart"
      OkButtonDisable={!cartItem.length}
    >
      {
        !cartItem.length ? <h3>Data not found</h3> :
          (
            <ModalMainWrapper>
              {cartItem.map((item: ICheckoutItem) => {
                return (
                  <CartItem
                    removeOnClick={handleRemoveItem(item)}
                    addOnClick={handleAddItem(item)}
                    removeButtonClick={handleRemoveItemFromCart(item)}
                    item={item}
                  />
                )})
              }
            </ModalMainWrapper>
          )
      }
    </CustomModal>
    <CustomModal
      title="Checkout"
      isOpen={modal.checkout}
      handleOkButton={handleCloseAllModal}
      handleCancelButton={handleModal('checkout')}
    >
      {
        !cartItem?.length ? <h1>Data not found</h1> :
          (
            <>
              <ModalMainWrapper>
                {checkoutData?.length > 0  && checkoutData?.map((item: ICheckoutItem) => {
                  return (
                  <ModalWrapper>
                    <CartImage src={item.imgUrl} alt="img" />
                    <span>Name : {item.name}</span>
                    <span>Price : {item.price}</span>
                  </ModalWrapper>
                )})
                }
                <h5>Total Tax: {totalText?.toString()}</h5>
                <h5>Total Amount: {totalAmount}</h5>
              </ModalMainWrapper>
            </>
          )
      }
    </CustomModal>
  </ProductWrapper>
  )
};

export default Product
