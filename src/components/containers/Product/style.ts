import styled from 'styled-components';

export const ProductWrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
  align-items: center;
  
  .cardWrapper {
    display: flex;
    gap: 14px;
    flex-wrap: wrap;
    align-items: center;
    justify-content: center;
    margin-top: 10px;
  }
`;

export const CartImage = styled.img`
  height: 45px;
  width: 60px;
  border-radius: 50%;
`;

export const ModalWrapper = styled.div`
  display: flex;
  flex-direction: row;
  border: 1px solid gainsboro;
  border-radius: 10px;
  align-items: center;
  gap: 25px;
  background: white;
  padding: 5px 20px
`;

export const ModalMainWrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
`;
