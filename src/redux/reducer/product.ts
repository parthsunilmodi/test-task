import {ADD_PRODUCT, REMOVE_CART_ITEM, REMOVE_PRODUCT} from '../selector/product';

interface ICheckoutItem {
  imgUrl: string,
  price: number,
  name: string,
  type: string,
  imported: string,
  tex: number,
  count: number,
  id: number
}


const initialState = {
  cartItem: []
};

export const productReducer = (state= initialState, action: any) => {
  switch (action.type) {
    case ADD_PRODUCT:
      let productArr: any = state.cartItem;
      const index = state.cartItem.findIndex((item: ICheckoutItem) => item.id === action.payload.id);
      if (index !== -1) {
        productArr = state.cartItem.map((item: any) => {
          if(item.id === action.payload.id) {
            return { ...item, count: item.count + 1 }
          }
          return item;
        })
      } else {
        productArr.push({ ...action.payload, count: 1 });
      }
    return { ...state, cartItem: productArr };
    
    case REMOVE_PRODUCT:
      let removeProductArr: any = state.cartItem;
      if (action.payload.count > 1) {
        removeProductArr = state.cartItem.map((item: ICheckoutItem) => {
          if (item.id === action.payload.id && item.count !== 0) {
            return {...item, count: item.count - 1}
          }
          return item
        });
      }
      return { ...state, cartItem: removeProductArr };
      
    case REMOVE_CART_ITEM:
      const cartArray = state.cartItem.filter((item: ICheckoutItem) => item.id !== action.payload.id);
      return ({
        ...state,
        cartItem: cartArray
      });
      
    default:
      return state
  }
};