import { ADD_PRODUCT, REMOVE_PRODUCT, REMOVE_CART_ITEM } from "../selector/product";


interface ICheckoutItem {
  imgUrl: string,
  price: string,
  name: string,
  type: string,
  imported: string,
  tex?: number,
  count?: number,
}

export const addProduct = (item :ICheckoutItem) => {
  return ({
    type: ADD_PRODUCT,
    payload: item
  });
};

export const removeProduct = (item: ICheckoutItem) => {
  return ({
    type: REMOVE_PRODUCT,
    payload: item
  })
};

export const removeProductFromCart = (item : ICheckoutItem) =>{
  return({
    type : REMOVE_CART_ITEM,
    payload : item
  })
};