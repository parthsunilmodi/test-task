import React from 'react';
import './App.css';
import Product from "./components/containers/Product";
import { Provider } from 'react-redux'
import store from "./redux/store"

function App() {
  return (
    <Provider store={store}>
    <div className="App">
      <Product />
    </div>
    </Provider>
  );
}

export default App;
